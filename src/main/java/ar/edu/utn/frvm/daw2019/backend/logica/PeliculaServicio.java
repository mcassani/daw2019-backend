package ar.edu.utn.frvm.daw2019.backend.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.edu.utn.frvm.daw2019.backend.dominio.Pelicula;
import ar.edu.utn.frvm.daw2019.backend.persistencia.PeliculaRepositorio;

@Service
public class PeliculaServicio {

	@Autowired
	private PeliculaRepositorio repositorio;
	
	public List<Pelicula> listarTodos() {
		return repositorio.findAll();
	}

	public Pelicula guardar(Pelicula p) {
		return repositorio.save(p);
	}

	public Page<Pelicula> listarTodosPaginados(Pageable pagina) {
		return repositorio.findAll(pagina);
	}

}
