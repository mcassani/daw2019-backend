package ar.edu.utn.frvm.daw2019.backend.persistencia;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.utn.frvm.daw2019.backend.dominio.Idioma;

public interface IdiomaRepositorio extends JpaRepository<Idioma, Integer> {

	public List<Idioma> findByNombre(String nombre);
	
	public List<Idioma> findByNombreIgnoreCase(String nombre);
	
	public List<Idioma> findByNombreIgnoreCaseContaining(String nombre);
	
}
