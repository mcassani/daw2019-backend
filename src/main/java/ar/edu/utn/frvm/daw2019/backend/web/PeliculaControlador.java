package ar.edu.utn.frvm.daw2019.backend.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.utn.frvm.daw2019.backend.dominio.Pelicula;
import ar.edu.utn.frvm.daw2019.backend.logica.PeliculaServicio;

@RestController
@RequestMapping("/peliculas")
public class PeliculaControlador {
	
	@Autowired
	private PeliculaServicio servicio;

	@RequestMapping
	public List<Pelicula> listarTodos() {
		return servicio.listarTodos();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public Pelicula guardar(@RequestBody Pelicula p) {
		return servicio.guardar(p);
	}
	
	@RequestMapping(params = {"page"})
	public Page<Pelicula> listarTodosPaginados(Pageable pagina) {
		return servicio.listarTodosPaginados(pagina);
	}
}
