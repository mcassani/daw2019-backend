package ar.edu.utn.frvm.daw2019.backend.persistencia;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.utn.frvm.daw2019.backend.dominio.Pelicula;

public interface PeliculaRepositorio extends JpaRepository<Pelicula, Integer> {

	public List<Pelicula> findByIdioma_Id(Integer id);
	
	public Page<Pelicula> findByIdioma_Id(Integer id, Pageable pagina);
	
}
