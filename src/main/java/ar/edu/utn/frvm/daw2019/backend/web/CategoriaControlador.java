package ar.edu.utn.frvm.daw2019.backend.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/saludar")
public class CategoriaControlador {
	
	@RequestMapping
	public String saludar() {
		return "Hola";
	}
}
