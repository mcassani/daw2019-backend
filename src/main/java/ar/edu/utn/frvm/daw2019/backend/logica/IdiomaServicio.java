package ar.edu.utn.frvm.daw2019.backend.logica;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.edu.utn.frvm.daw2019.backend.dominio.Idioma;
import ar.edu.utn.frvm.daw2019.backend.dominio.Pelicula;
import ar.edu.utn.frvm.daw2019.backend.persistencia.IdiomaRepositorio;
import ar.edu.utn.frvm.daw2019.backend.persistencia.PeliculaRepositorio;

@Service
public class IdiomaServicio {

	@Autowired
	private IdiomaRepositorio repositorio;
	
	@Autowired
	private PeliculaRepositorio repositorioPelicula;
	
	public List<Idioma> listarTodos() {
		return repositorio.findAll();		
	}
	
	public Optional<Idioma> get(Integer id) {
		return repositorio.findById(id);
	}

	public Idioma guardar(Idioma i) {
		return repositorio.save(i);
	}

	public Idioma actualizar(Idioma i) {
		return repositorio.save(i);
	}

	public void eliminar(Integer id) {
		repositorio.deleteById(id);
	}

	public List<Idioma> listrarFiltradoPorNombre(String nombre) {
		if (nombre.length() < 3) {
			throw new RuntimeException("Debe ingresar minimo 3 letras");
		}
		return repositorio.findByNombreIgnoreCaseContaining(nombre);
	}

	public Page<Pelicula> getPeliculasPorIdioma(Integer id, Pageable pagina) {
		return repositorioPelicula.findByIdioma_Id(id, pagina);
	}


}
