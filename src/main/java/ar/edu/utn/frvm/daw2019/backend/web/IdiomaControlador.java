package ar.edu.utn.frvm.daw2019.backend.web;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.utn.frvm.daw2019.backend.dominio.Idioma;
import ar.edu.utn.frvm.daw2019.backend.dominio.Pelicula;
import ar.edu.utn.frvm.daw2019.backend.logica.IdiomaServicio;

@RestController
@RequestMapping("/idiomas")
public class IdiomaControlador {

	@Autowired
	private IdiomaServicio servicio;

	@RequestMapping
	public List<Idioma> listarTodos() {
		return servicio.listarTodos();
	}

	@RequestMapping(params = {"nombre"})
	public List<Idioma> listrarFiltradoPorNombre(@RequestParam(name = "nombre") String nombre) {
		return servicio.listrarFiltradoPorNombre(nombre);
	}

	@RequestMapping("/{ID}")
	public Optional<Idioma> get(@PathVariable(name = "ID") Integer id) {
		return servicio.get(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public Idioma guardar(@RequestBody Idioma i) {
		return servicio.guardar(i);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public Idioma actualizar(@RequestBody Idioma i) {
		return servicio.actualizar(i);
	}

	@RequestMapping(value = "/{ID}", method = RequestMethod.DELETE)
	public void eliminar(@PathVariable(name = "ID") Integer id) {
		servicio.eliminar(id);
	}
	
	@RequestMapping(value = "/{ID}/peliculas")	
	public Page<Pelicula> getPelicuasPorIdioma(@PathVariable(name = "ID") Integer id, Pageable pagina) {
		return servicio.getPeliculasPorIdioma(id, pagina);
	}

}
