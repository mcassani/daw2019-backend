package ar.edu.utn.frvm.daw2019.backend.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "pelicula")
@Data
public class Pelicula {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String titulo;
	private String comentario;
	@ManyToOne
	private Idioma idioma;
}
