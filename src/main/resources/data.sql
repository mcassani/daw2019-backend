INSERT INTO idioma (id, nombre, descripcion) VALUES (1, 'Español', 'Nuestro Idioma');
INSERT INTO idioma (id, nombre, descripcion) VALUES (2, 'Ingles', 'Es muy dificil');

INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (100, 'Pelicula11', 'Comentario1', 1);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (101, 'Pelicula12', 'Comentario2', 2);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (102, 'Pelicula13', 'Comentario1', 1);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (103, 'Pelicula14', 'Comentario2', 2);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (104, 'Pelicula15', 'Comentario1', 1);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (105, 'Pelicula16', 'Comentario2', 2);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (106, 'Pelicula17', 'Comentario1', 1);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (107, 'Pelicula18', 'Comentario2', 2);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (108, 'Pelicula19', 'Comentario1', 1);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (109, 'Pelicula20', 'Comentario2', 2);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (110, 'Pelicula21', 'Comentario1', 1);
INSERT INTO pelicula (id, titulo, comentario, idioma_id) VALUES (111, 'Pelicula22', 'Comentario2', 2);